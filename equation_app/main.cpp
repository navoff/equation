#include <iostream>
#include <string>

#include "../equation_lib/parser.h"

int main()
{
    using namespace std;

    while (true)
    {
        cout << "Print equation (for example 13+5*x=10) or \"quit\" for exit: ";

        // чтение входной строки
        string str;
        getline(cin, str);

        if (str == "quit")
        {
            return 0;
        }

        try
        {
            cout << "x = " << parser::solve_equation(str) << endl;
        }
        catch (exception const& e)
        {
            cout << "Error: " << e.what() << endl;
        }
        catch (...)
        {
            cout << "syntax error" << endl;
        }
    }

    return 0;
}

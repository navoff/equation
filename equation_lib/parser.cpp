#include "parser.h"
#include "operators.h"
#include <stack>
#include <algorithm>

/**
 * @brief Вычислить значение выражения
 * @param text - строка, содержащая выражение.
 * @return Полученное значение
 *
 * Допустимые символы:
 *   - операторы: + - * /
 *   - целые десятичные цифры: 0 1 2 .. 9
 * Скобки не допускаются
 * Числа с плавающей точкой не допускаются
 * Пробелы не допускаются
 * Допускается начинать выражения со знака "-". Например, -10*2+3
 */
float parser::calc_expression(const std::string& text)
{
    // в начале выражения допускаются только цифры или знак '-'
    if ((isdigit(text[0]) == false) && (text[0] != '-'))
    {
        throw std::exception("syntax error");
    }

    auto operator_stack = std::stack<operator_base*>();
    auto calc_stack = std::stack<float>();

    size_t idx = 0;
    uint32_t number = 0;

    while (idx < text.size())
    {
        const auto sym = text[idx++];

        if (isdigit(sym))
        {
            number = number * 10 + sym - '0';
        }
        else
        {
            // если встретили не цифру
            calc_stack.push(number);
            number = 0;

            // значит встретили оператор
            auto next_operator = get_operator(sym);

            while (operator_stack.empty() == false)
            {
                auto prev_operator = operator_stack.top();
                // если у нового оператора приоритет выше, чем у последнего сохранённого
                if (next_operator->get_priority() > prev_operator->get_priority())
                {
                    // операнды не трогаем, продолжаем анализ строки
                    break;
                }

                // т.к. пошёл новый оператор,
                // значит для предыдущего оператора накопили нужно кол-во операндов
                prev_operator->execute(calc_stack);
                operator_stack.pop();
                delete prev_operator;
            }

            // сохранить оператор, пока не получим нужно кол-во операндов для него
            operator_stack.push(next_operator);
        }
    }

    calc_stack.push(number);
    // выполнить все не выполненые операнды
    while (operator_stack.empty() == false)
    {
        const auto top_operator = operator_stack.top();
        operator_stack.pop();
        (*top_operator).execute(calc_stack);
        delete top_operator;
    }

    return calc_stack.top();
}

operator_base* parser::get_operator(const char sym)
{
    switch (sym)
    {
    case '+': return new operator_plus();
    case '-': return new operator_minus();
    case '*': return new operator_mul();
    case '/': return new operator_div();
    default: throw std::exception("syntax error");
    }
}

/**
 * @brief Решить уравнение с одной неизвестной x
 * @return Значение x - решение уравнения
 *
 * Неизвестный x должен встречаться в уравнении только один раз.
 * Допустимые операторы: + - * /
 * Скобки не допускаются
 * Примеры уравнений:
 *    13+5*x=10
 *    10=5/x-13
 */
float parser::solve_equation(const std::string& text)
{
    using namespace std;

    // удалить все пробелы
    auto str = text;
    str.erase(remove_if(str.begin(), str.end(), isspace), str.end());

    const auto eq_pos = str.find('=');
    if (eq_pos == string::npos) throw exception("syntax error: symbol \"=\" not found");

    auto x_pos = str.find('x');
    if (x_pos == string::npos) throw exception("syntax error: symbol \"x\" not found");

    // разделим уравнение по знаку "=" на 2 выражения
    string x_str; // часть уравнения, включающее x
    string c_str; // часть уравнения, НЕ включающее x
    ((x_pos < eq_pos) ? x_str : c_str) = str.substr(0, eq_pos);  // подстока слева от "="
    ((x_pos < eq_pos) ? c_str : x_str) = str.substr(eq_pos + 1); // подстока справа от "="
    x_pos = x_str.find('x');

    // c = b + k * x
    //
    // пусть y(x) = b + k * x, где b и k - неизвестны
    // тогда y_0 = y(x = 0)
    //       y_1 = y(x = 1)
    //       y_2 = y(x = 2)

    x_str[x_pos] = '0';
    const auto y_0 = calc_expression(x_str);
    x_str[x_pos] = '1';
    const auto y_1 = calc_expression(x_str);
    x_str[x_pos] = '2';
    const auto y_2 = calc_expression(x_str);

    const auto c = calc_expression(c_str);

    if (isinf(y_0))
    {
        // для уравнения вида: y(x) = b + k / x

        const auto b = 2 * y_2 - y_1;
        const auto k = 2 * y_1 - 2 * y_2;
        const auto x = k / (c - b);
        return x;
    }
    else
    {
        // для уравнения вида: y(x) = b + k * x
        const auto b = y_0;
        const auto k = y_1 - y_0;
        const auto x = (c - b) / k;
        return x;
    }
}
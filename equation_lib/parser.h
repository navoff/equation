#pragma once
#include <string>
#include "operators.h"

class parser
{
public:
    static float calc_expression(const std::string& text);
    static float solve_equation(const std::string& text);

private:
    static operator_base* get_operator(char sym);
};


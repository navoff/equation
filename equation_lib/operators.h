#pragma once
#include <iostream>
#include <stack>

using calc_stack = std::stack<float>;

class operator_base
{
public:
    operator_base() = default;
    virtual ~operator_base() = default;
    virtual void execute(calc_stack& calc_stack) = 0;
    virtual int get_priority() = 0;
};

class operator_plus : public operator_base
{
public:
    void execute(calc_stack& calc_stack) override
    {
        const auto arg2 = calc_stack.top();
        calc_stack.pop();

        const auto arg1 = calc_stack.top();
        calc_stack.pop();

        calc_stack.push(arg1 + arg2);
    }

    int get_priority() override { return 0; }
};

class operator_minus : public operator_base
{
public:
    void execute(calc_stack& calc_stack) override
    {
        const auto arg2 = calc_stack.top();
        calc_stack.pop();

        const auto arg1 = calc_stack.top();
        calc_stack.pop();

        calc_stack.push(arg1 - arg2);
    }

    int get_priority() override { return 0; }
};

class operator_mul : public operator_base
{
public:
    void execute(calc_stack& calc_stack) override
    {
        const auto arg2 = calc_stack.top();
        calc_stack.pop();

        const auto arg1 = calc_stack.top();
        calc_stack.pop();

        calc_stack.push(arg1 * arg2);
    }

    int get_priority() override { return 1; }
};

class operator_div : public operator_base
{
public:
    void execute(calc_stack& calc_stack) override
    {
        const auto arg2 = calc_stack.top();
        calc_stack.pop();

        const auto arg1 = calc_stack.top();
        calc_stack.pop();

        calc_stack.push(arg1 / arg2);
    }

    int get_priority() override { return 1; }
};

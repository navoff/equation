#include "pch.h"
#include "../equation_lib/parser.h"

namespace equation
{
    TEST(calc_expression, simple_plus_minus)
    {
        EXPECT_EQ(parser::calc_expression("10+3"), 10 + 3);
        EXPECT_EQ(parser::calc_expression("220-1000"), 220 - 1000);
        EXPECT_EQ(parser::calc_expression("-100000-1000"), -100000 - 1000);
    }

    TEST(calc_expression, complex_plus_minus)
    {
        EXPECT_EQ(parser::calc_expression("1000-42+8"), 1000 - 42 + 8);
        EXPECT_EQ(parser::calc_expression("-220-1000"), -220 - 1000);
    }

    TEST(calc_expression, simple_mul)
    {
        EXPECT_EQ(parser::calc_expression("16*389"), 16 * 389);
        EXPECT_EQ(parser::calc_expression("-16*389"), -16 * 389);
        EXPECT_EQ(parser::calc_expression("6224*0"), 6224 * 0);
    }

    TEST(calc_expression, simple_div)
    {
        EXPECT_EQ(parser::calc_expression("4/2"), 4.f / 2);
        EXPECT_EQ(parser::calc_expression("-10/3"), -10.f / 3);
    }

    TEST(calc_expression, complex_mul_div)
    {
        EXPECT_EQ(parser::calc_expression("-10*399/76"), -10.f * 399 / 76);
        EXPECT_EQ(parser::calc_expression("100000/4/5/6"), 100000.f / 4 / 5 / 6);
        EXPECT_EQ(parser::calc_expression("100000/4/5*6"), 100000.f / 4 / 5 * 6);
    }

    TEST(solve_equation, simple)
    {
        EXPECT_EQ(parser::solve_equation("x = 1000"), 1000.f);
        EXPECT_EQ(parser::solve_equation("1000 = x"), 1000.f);
        EXPECT_EQ(parser::solve_equation("2 * x = 1000"), 1000.f / 2);
        EXPECT_EQ(parser::solve_equation("1000 = 2 * x * 100"), 1000.f / 200);
        EXPECT_EQ(parser::solve_equation("x + 100 = 1000"), 1000.f - 100);
        EXPECT_EQ(parser::solve_equation("1000 = -x + 100"), -1000.f + 100);
    }

    TEST(solve_equation, complex)
    {
        EXPECT_EQ(parser::solve_equation("13 + 5 * x = 10"), -0.6f);
        EXPECT_EQ(parser::solve_equation("26 / 2 + 100 * x / 20 = 900 / 100 + 1"), -0.6f);
    }

    TEST(solve_equation, complex_converse)
    {
        EXPECT_EQ(parser::solve_equation("10 / x = 1"), 10.f);
        EXPECT_EQ(parser::solve_equation("10 / x * 5 + 10 = 0"), -5.f);
        EXPECT_EQ(parser::solve_equation("56 / 8 - 7 = 10 / x * 5 + 10"), -5.f);
    }
}
